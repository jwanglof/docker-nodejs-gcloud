FROM node:lts-alpine3.21

LABEL org.opencontainers.image.authors="johan@botillsammans.nu"

RUN mkdir -p /opt
WORKDIR /opt

RUN apk add --no-cache \
    bash \
    ca-certificates \
    curl \
    git \
    openssl  \
    openssh-client \
    python3 \
    tar \
    gzip

ENV NODE_OPTIONS=openssl-legacy-provider
ENV CLOUDSDK_PYTHON_SITEPACKAGES=1

RUN curl https://sdk.cloud.google.com > install.sh \
    && chmod +x install.sh \
    && ./install.sh --disable-prompts --install-dir=/opt

RUN mkdir ${HOME}/.ssh
ENV PATH=/opt/google-cloud-sdk/bin:$PATH

WORKDIR /root

ENV DOCKER_VERSION=28.0.0
RUN set -x \
    && curl -fSL "https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz" -o docker.tgz \
    && tar -xzvf docker.tgz \
    && mv docker/* /usr/local/bin/ \
    && rmdir docker \
    && rm docker.tgz \
    && docker -v \
    && apk del curl openssl \
    && corepack enable pnpm \
    && corepack use pnpm@latest

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
#CMD ["sh"]
